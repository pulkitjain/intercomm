#include <iostream>
#include <fstream>
#include <string>
#include "intercomm.h"

using namespace std;

int main() {
    string str;
    std::ifstream file("names.txt");
    while (std::getline(file, str)) {
       //cout << str << endl;
       string firstName = str.substr(0, str.find(' '));
       string lastName = str.substr(str.find(' ') + 1);
       //cout << " " << firstName << " + " << lastName << endl;
       convertToPhone(firstName, lastName);
    }
    return 0;
}
