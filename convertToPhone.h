#include <iostream>
#include <string>
#include <map>
#include "addToMap.h"

using namespace std;

map<char, string> generateMap() {
    map<char, string> t9;

    t9['a'] = "2";
    t9['b'] = "2";
    t9['c'] = "2";
    t9['d'] = "3";
    t9['e'] = "3";
    t9['f'] = "3";
    t9['g'] = "4";
    t9['h'] = "4";
    t9['i'] = "4";
    t9['j'] = "5";
    t9['k'] = "5";
    t9['l'] = "5";
    t9['m'] = "6";
    t9['n'] = "6";
    t9['o'] = "6";
    t9['p'] = "7";
    t9['q'] = "7";
    t9['r'] = "7";
    t9['s'] = "7";
    t9['t'] = "8";
    t9['u'] = "8";
    t9['v'] = "8";
    t9['w'] = "9";
    t9['x'] = "9";
    t9['y'] = "9";
    t9['z'] = "9";
    t9['0'] = "0";

    return t9;
}

string getT9(string s, map<char, string> m) {
    string output;
    //cout << s << endl;
    for (int i = 0; i < s.size(); i++) {
        output += m.find(s[i])->second;
    }
//    cout << output << endl;
    return output;
}

string getPassString(string first, string last, int pass) {
    
    string s;

    if (pass == 0) {
        return first.substr(0,3) + last.substr(0,2);
    } else if (pass == 1) {
        return first.substr(0,2) + last.substr(0,3);
    } else if (pass == 2) {
        return first.substr(0,1) + last.substr(0,4);
    } else {
        return first.substr(0,2) + last.substr(0,2) + "0";
    }
}

string str_tolower(string str) {
    string lower;
    for (int i = 0; i < str.size(); ++i) {
        str[i] = tolower(str[i]);
    }
    lower = str;
    return lower;
}

string convertToPhone(string first, string last) {

    map<char, string> t9map;
    t9map = generateMap();
    
    first = str_tolower(first);
    last = str_tolower(last);

    int pass = 0;
    string lookUp = getPassString(first, last, pass);
    string t9text = getT9(lookUp, t9map);
   /* 
    first[0] = tolower(first[0]);
    last[0] = tolower(last[0]);
    */
    while (!addToMap(t9text, first, last)) {
        pass ++;
        lookUp = getPassString(first, last, pass);
        t9text = getT9(lookUp, t9map);
        if(pass == 1) break;
    }
    //cout << t9text << " ";
    return t9text;
}

