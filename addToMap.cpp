#include <iostream>
#include <fstream>
#include <string>
#include "intercomm.h"

using namespace std;

bool addToMap(string number, string first, string last){
    
    cout << number << endl;

    fstream fileHandle("directory.dir", std::fstream::in);
    string numberFromFile;
    bool isUnique = true;
    while (getline(fileHandle, numberFromFile)) {
        numberFromFile = numberFromFile.substr(numberFromFile.find('|') + 1);
        //cout << numberFromFile << endl;
        if (numberFromFile.compare(number) == 0) {
            isUnique = false;
            break;
        }
    }
    fileHandle.close();
    //cout << isUnique << endl;
    if(isUnique) {
        fstream fileHandle("directory.dir", std::fstream::app | std::fstream::out);
        fileHandle << first << " " << last << "|" << number << "\n";
        fileHandle.close();
        return true;
    }
    return false;
}
